from prometheus_client import Alert, AlertManager, start_http_server

alert_manager = AlertManager()
alert = Alert(
    "HighCPUUSsage",
    "The usage of the CPU is too high",
    expr='sum(node_namespace_pod_container:container_cpu_usage_seconds_total:sum_irate{namespace="emojivoto", container="web-svc"}) by (container) > 0.0125',
    for_duration='1m',
)
alert_manager.register(alert)
start_http_server(8000)
